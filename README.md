# charity-recommender-system-model-serving

Repo serving models trained in [this repo](https://gitlab.com/Learningsome1/charity-recommender-system).

How to run the FastAPI/uvicorn app directly:
* run `poetry install`, then `poetry shell`
* make sure the necessary *.env* file is present in the root dir of the project
* from *src/*, execute `uvicorn inference:app --reload --host 0.0.0.0 --port 8001`. It's important to set the host to 0.0.0.0 as it doesn't work with localhost or 127.0.0.1 otherwise. Port 8000 may be in use, so resort to a free one such as 8001.

Using Docker:
* run `docker build -t fastapi-app -f docker/Dockerfile .` from *src/* to build the image. Using `-f` enables you to specify the path to the Dockerfile, while staying in the root dir of the project (the `.` at the end of the command specifies the build context, which is the current directory. It tells Docker to include all the files and directories in the current directory as part of the build context.). Basically this enables you to avoid using '..' in the Dockerfile to refer to files in the parent dir
* run `docker run -p 8001:8001 fastapi-app` from the root dir of the project (or do the same using portainer) to create and run the container

Having run the container, navigate to *\<vm-public-ip>:8001/docs* to open the Swagger UI interace for interacting with the model using FastAPI.